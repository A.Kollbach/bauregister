export * from './lego.service';
import { LegoService } from './lego.service';
export * from './swagger.service';
import { SwaggerService } from './swagger.service';
export * from './users.service';
import { UsersService } from './users.service';
export const APIS = [LegoService, SwaggerService, UsersService];
