export class IsLoading {
  static readonly type = '[App] is loading';

  constructor(public payload: boolean) {}
}
