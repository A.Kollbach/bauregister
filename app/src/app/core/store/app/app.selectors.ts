import { Selector } from '@ngxs/store';
import { AppStateModel } from './app.state.model';
import { AppState } from './app.state';

export class AppSelectors {
  @Selector([AppState])
  static app(state: AppStateModel) {
    return state;
  }
}
