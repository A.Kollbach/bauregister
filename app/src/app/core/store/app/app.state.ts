import { AppStateModel } from './app.state.model';
import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { IsLoading } from './app.action';

@State<AppStateModel>({
  name: 'app',
  defaults: {
    isLoading: false,
  },
})
@Injectable()
export class AppState {
  /* Methods */
  @Action(IsLoading)
  isLoading(ctx: StateContext<AppStateModel>, action: IsLoading): void {
    ctx.patchState({ isLoading: action.payload });
  }
}
