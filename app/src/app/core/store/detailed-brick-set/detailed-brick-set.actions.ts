export class GetDetailedBrickSet  {
  static readonly type = '[DetailedBrickSet] Load Detailed Brick Set';
  constructor(public brickSetNumber: string) {}
}

export class Reset {
  static readonly type = '[DetailedBrickSet] Reset ';
}

