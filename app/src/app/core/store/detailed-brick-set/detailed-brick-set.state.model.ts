import {Brick} from "../../../shared/interfaces/brick";
import {Theme} from "../../../shared/interfaces/theme";
import {Color} from "../../../shared/interfaces/color";

export interface DetailedBrickSetStateModel {
  brickSetNumber: string;
  Name: string;
  theme?: Theme;
  set_img_url: string;
  set_url: string;
  bricks?: Brick[];
  spareBricks?: Brick[];
  Colors?: Color[];
}
