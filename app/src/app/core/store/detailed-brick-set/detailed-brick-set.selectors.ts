import {Selector} from "@ngxs/store";
import {DetailedBrickSetStateModel} from "./detailed-brick-set.state.model";
import {DetailedBrickSetState} from "./detailed-brick-set.state";

export class DetailedBrickSetSelectors{

  @Selector([DetailedBrickSetState])
  static getDetailedBrickSet(state: DetailedBrickSetStateModel) {
    return state;
  }
}
