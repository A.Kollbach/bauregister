import {DetailedBrickSetStateModel} from "./detailed-brick-set.state.model";
import {Action, State, StateContext} from "@ngxs/store";
import {Injectable} from "@angular/core";
import {RebrickableService} from "../../../shared/services/rebrickable.service";
import {GetDetailedBrickSet, Reset} from "./detailed-brick-set.actions";

@State<DetailedBrickSetStateModel>({
  name: 'detailedBrickSet',
  defaults: {
    brickSetNumber: '',
    Name: '',
    set_img_url: '',
    set_url: '',
    theme: undefined,
    bricks: [],
    spareBricks: [],
    Colors: []
  },
}) @Injectable()
export class DetailedBrickSetState {

  /*Constructor*/
  constructor(private rebrickableService: RebrickableService) {
  }

  /*Methods*/
  @Action(Reset)
  resetDetailedBrickSet(ctx:
                          StateContext<DetailedBrickSetStateModel>): void {
    ctx.patchState({
      brickSetNumber: '',
      Name: '',
      set_img_url: '',
      set_url: '',
      theme: undefined,
      bricks: [],
      spareBricks: [],
      Colors: []
    });
  }

  @Action(GetDetailedBrickSet)
  getDetailedBrickSet(ctx: StateContext<DetailedBrickSetStateModel>, action: GetDetailedBrickSet) {
    const {brickSetNumber} = action;

    ctx.patchState({
    });
  }
}
