import {BrickSet} from "../../../shared/interfaces/brick-set";
import {Brick} from "../../../shared/interfaces/brick";
import {Color} from "../../../shared/interfaces/color";
import {Theme} from "../../../shared/interfaces/theme";

export interface BrickSetStatusStateModel {
  isLoading: boolean;
  brickSetNumber: string;
  brickSet?: BrickSet;
  bricks?: Brick[];
  spareBricks?: Brick[];
  brickSetColors?:Color[];
  completeBricks?: Brick[];
  lostBricks?: Brick[];
  theme?: Theme
}
