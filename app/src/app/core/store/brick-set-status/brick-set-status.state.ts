import { BrickSetStatusStateModel } from './brick-set-status.state.model';
import { Injectable } from '@angular/core';
import { Action, State, StateContext } from '@ngxs/store';
import { RebrickableService } from '../../../shared/services/rebrickable.service';
import { SetBrickSetStatus } from './brick-set-status.actions';
import { forkJoin } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Brick } from '../../../shared/interfaces/brick';
import { BrickSetStatus } from '../../../shared/interfaces/brick-set-status';
import { Color } from '../../../shared/interfaces/color';

@State<BrickSetStatusStateModel>({
  name: 'brickSetStatus',
  defaults: {
    isLoading: false,
    brickSetNumber: '',
    brickSet: undefined,
    bricks: [],
    spareBricks: [],
    brickSetColors: [],
    completeBricks: [],
    lostBricks: [],
    theme: undefined,
  },
})
@Injectable()
export class BrickSetStatusState {
  /* Constructor */
  constructor(private rebrickableService: RebrickableService) {}

  /* Methods */
  @Action(SetBrickSetStatus)
  setBrickSetStatus(ctx: StateContext<BrickSetStatusStateModel>, action: SetBrickSetStatus): void {
    ctx.patchState({ isLoading: true });
    const setId = action.payload;
    const brickSetStatus = new BrickSetStatus(setId);

    this.rebrickableService
      .getSet(brickSetStatus.brickSetNumber)
      .pipe(
        switchMap((brickSet) => {
          brickSetStatus.brickSet = brickSet;

          return forkJoin({
            theme: this.rebrickableService.getTheme(brickSet.theme_id),
            parts: this.rebrickableService.getSetParts(brickSetStatus.brickSetNumber, undefined, 1000),
          });
        }),
        map(({ theme, parts }) => {
          brickSetStatus.theme = theme;
          brickSetStatus.bricks = parts.results.filter((b: Brick) => !b.is_spare);
          brickSetStatus.spareBricks = parts.results.filter((b: Brick) => b.is_spare);
          brickSetStatus.brickSetColors = this.sortColorsByRGB(this.getSetColors(parts.results));

          return brickSetStatus;
        }),
      )
      .subscribe((updatedBrickSetStatus) => {
        if (updatedBrickSetStatus) {
          console.log('updatedBrickSetStatus', updatedBrickSetStatus);
          ctx.setState({
            brickSetNumber: updatedBrickSetStatus.brickSetNumber,
            brickSet: updatedBrickSetStatus.brickSet,
            bricks: updatedBrickSetStatus.bricks,
            spareBricks: updatedBrickSetStatus.spareBricks,
            brickSetColors: updatedBrickSetStatus.brickSetColors,
            theme: updatedBrickSetStatus.theme,
            isLoading: false,
          });
        }
      });
  }

  private getSetColors(parts: Brick[]): Color[] {
    return parts
      .map((b: Brick) => b.color)
      .reduce((acc: Color[], current: Color) => {
        const x = acc.find((item) => item.name === current.name);
        if (!x && current.id !== 9999 && !current.name.includes('Trans')) {
          acc.push(current);
        }
        return acc;
      }, []);
  }

  private sortColorsByRGB(colors: Color[]): Color[] {
    return colors.sort((a, b) => {
      const rgbA = parseInt(a.rgb, 16);
      const rgbB = parseInt(b.rgb, 16);
      return rgbA - rgbB;
    });
  }
}
