import {Selector} from "@ngxs/store";
import {BrickSetStatusStateModel} from "./brick-set-status.state.model";
import { BrickSetStatusState } from "./brick-set-status.state";

export class BrickSetStatusSelectors{
  @Selector([BrickSetStatusState])
  static brickSetStatus(state: BrickSetStatusStateModel) {
    return state;
  }
}
