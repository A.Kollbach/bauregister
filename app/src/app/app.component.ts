import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {MatToolbar} from "@angular/material/toolbar";
import {MainNavComponent} from "./modules/main-frame/main-nav/main-nav.component";
import {MainHomeComponent} from "./modules/main-frame/main-home/main-home.component";
import {LoggingService} from "./shared/services/logging.service";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, MatToolbar, MainNavComponent, MainHomeComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  title = 'BauRegister';

  constructor(private loggingService: LoggingService) {

  }

}
