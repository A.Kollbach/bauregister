import { Component, signal } from '@angular/core';
import { MatButton } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormField } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { FormBuilder, FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { RebrickableService } from '../../../shared/services/rebrickable.service';
import { BrickSet } from '../../../shared/interfaces/brick-set';
import { MatIcon } from '@angular/material/icon';
import { MatSlideToggle } from '@angular/material/slide-toggle';
import { MatCheckbox } from '@angular/material/checkbox';
import { NgOptimizedImage } from '@angular/common';
import { RandomBorderDirective } from '../../../shared/directives/random-border.directive';
import { Router } from '@angular/router';
import { MatExpansionModule } from '@angular/material/expansion';
import { Store } from '@ngxs/store';
import { IsLoading } from '../../../core/store/app/app.action';
import { LoadingDirective } from '../../../shared/directives/loading.directive';

@Component({
  selector: 'br-search',
  standalone: true,
  imports: [
    ReactiveFormsModule,
    MatCardModule,
    MatFormField,
    MatButton,
    MatInput,
    MatIcon,
    MatSlideToggle,
    FormsModule,
    MatCheckbox,
    NgOptimizedImage,
    RandomBorderDirective,
    MatExpansionModule,
    LoadingDirective,
  ],
  templateUrl: './search.component.html',
  styleUrl: './search.component.scss',
})
export class SearchComponent {
  readonly panelOpenState = signal(true);
  searchForm: FormGroup;
  brickSets = signal<BrickSet[]>([]);

  constructor(
    private formBuilder: FormBuilder,
    private rebrickableService: RebrickableService,
    private router: Router,
    private store: Store,
  ) {
    this.searchForm = this.formBuilder.group({
      searchTerm: new FormControl('', [Validators.required]),
    });
  }

  getBrickList(set_num: string) {
    this.store.dispatch(new IsLoading(true));
    this.router.navigate(['bricklist', set_num]).then();
  }

  protected searchSets(form: FormGroup) {
    this.store.dispatch(new IsLoading(true));
    this.panelOpenState.set(false);
    this.rebrickableService.getSets(form.value.searchTerm).subscribe((res) => {
      this.brickSets.set(res.results);
      this.store.dispatch(new IsLoading(false));
    });
  }
}
