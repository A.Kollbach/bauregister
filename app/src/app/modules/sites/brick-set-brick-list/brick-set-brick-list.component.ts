import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MatSidenav, MatSidenavContainer } from '@angular/material/sidenav';
import { Brick } from '../../../shared/interfaces/brick';
import { RandomBorderDirective } from '../../../shared/directives/random-border.directive';
import { MatCheckbox } from '@angular/material/checkbox';
import { MatButtonToggle, MatButtonToggleGroup } from '@angular/material/button-toggle';
import { BrickComponent } from '../../elements/brick/brick.component';
import { MatTab, MatTabBody, MatTabGroup } from '@angular/material/tabs';
import { MatCard, MatCardContent, MatCardTitle } from '@angular/material/card';
import { MatGridList } from '@angular/material/grid-list';
import { MatMiniFabButton } from '@angular/material/button';
import { MatIcon } from '@angular/material/icon';
import { Store } from '@ngxs/store';
import { BrickSetStatusSelectors } from '../../../core/store/brick-set-status/brick-set-status.selectors';
import { BrickSetStatusStateModel } from '../../../core/store/brick-set-status/brick-set-status.state.model';
import { AsyncPipe, NgIf } from '@angular/common';
import { SetBrickSetStatus } from '../../../core/store/brick-set-status/brick-set-status.actions';
import { NgxPrintDirective, NgxPrintService, PrintOptions } from 'ngx-print';
import { IsLoading } from '../../../core/store/app/app.action';

@Component({
  selector: 'br-brick-set-brick-list',
  standalone: true,
  imports: [
    MatSidenav,
    RandomBorderDirective,
    MatCheckbox,
    MatButtonToggleGroup,
    MatButtonToggle,
    BrickComponent,
    MatTabGroup,
    MatTab,
    MatCardTitle,
    MatCard,
    MatCardContent,
    MatSidenavContainer,
    MatGridList,
    MatTabBody,
    MatMiniFabButton,
    MatIcon,
    AsyncPipe,
    NgxPrintDirective,
    NgIf,
  ],
  templateUrl: './brick-set-brick-list.component.html',
  styleUrl: './brick-set-brick-list.component.scss',
})
export class BrickSetBrickListComponent implements OnInit {
  brickSetStatus: BrickSetStatusStateModel | undefined;
  printMode: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private store: Store,
    private printService: NgxPrintService,
  ) {}

  ngOnInit(): void {
    this.store.dispatch(new SetBrickSetStatus(this.route.snapshot.params['id']));
    this.store.select(BrickSetStatusSelectors.brickSetStatus).subscribe((x) => {
      this.brickSetStatus = x;
      this.store.dispatch(new IsLoading(false));
    });
  }

  getTotalQuantity(count: Brick[]): number {
    if (!count) {
      return 0;
    }
    return count.reduce((total, brick) => total + brick.quantity, 0);
  }

  printPartList() {
    this.printMode = true;
    const printOptions = new PrintOptions({
      printSectionId: 'print-section',
      useExistingCss: true,
      closeWindow: false,
    });
    this.printService.print(printOptions);
    this.printMode = false;
  }
}
