import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrickSetBrickListComponent } from './brick-set-brick-list.component';

describe('BrickSetBrickListComponent', () => {
  let component: BrickSetBrickListComponent;
  let fixture: ComponentFixture<BrickSetBrickListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BrickSetBrickListComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BrickSetBrickListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
