import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrickSetComponent } from './brick-set.component';

describe('BrickSetComponent', () => {
  let component: BrickSetComponent;
  let fixture: ComponentFixture<BrickSetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BrickSetComponent]
    })
    .compileComponents();

    fixture = TestBed.createComponent(BrickSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
