import { booleanAttribute, Component, Input } from '@angular/core';
import { Brick } from '../../../shared/interfaces/brick';
import { NgClass } from '@angular/common';
import { MatCheckbox } from '@angular/material/checkbox';

@Component({
  selector: 'br-brick',
  standalone: true,
  imports: [NgClass, MatCheckbox],
  templateUrl: './brick.component.html',
  styleUrl: './brick.component.scss',
})
export class BrickComponent {
  @Input() brick!: Brick;
  @Input({ transform: booleanAttribute }) printMode: boolean = false;
}
