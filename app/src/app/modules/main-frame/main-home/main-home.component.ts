import { Component } from '@angular/core';
import {MainNavComponent} from "../main-nav/main-nav.component";
import {RouterOutlet} from "@angular/router";

@Component({
  selector: 'br-main-home',
  standalone: true,
  imports: [
    MainNavComponent,
    RouterOutlet
  ],
  templateUrl: './main-home.component.html',
  styleUrl: './main-home.component.scss'
})
export class MainHomeComponent {

}
