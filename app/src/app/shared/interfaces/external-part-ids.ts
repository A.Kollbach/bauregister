export interface ExternalPartIds {
  BrickLink: string[];
  BrickOwl: string[];
  Brickset: string[];
  LDraw: string[];
  LEGO: string[];
}
