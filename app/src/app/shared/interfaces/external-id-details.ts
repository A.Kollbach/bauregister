export interface ExternalIdDetails {
  ext_ids: (number | null)[];
  ext_descrs: string[][];
}
