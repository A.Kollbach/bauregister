import {ExternalIdDetails} from "./external-id-details";

export interface ExternalColorIds {
  BrickLink: ExternalIdDetails;
  BrickOwl: ExternalIdDetails;
  LEGO: ExternalIdDetails;
  Peeron: ExternalIdDetails;
  LDraw: ExternalIdDetails;
}
