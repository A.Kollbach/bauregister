import {ExternalColorIds} from "./external-color-ids";

export interface Color {
  id: number;
  name: string;
  rgb: string;
  is_trans: boolean;
  external_ids: ExternalColorIds;
}
