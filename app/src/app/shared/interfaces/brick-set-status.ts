import {BrickSet} from "./brick-set";
import {Brick} from "./brick";
import {Theme} from "./theme";
import {Color} from "./color";

export class BrickSetStatus {
  constructor(brickSetNumber: string) {
    this.brickSetNumber = brickSetNumber;
  }

  brickSetNumber: string;
  brickSet?: BrickSet;
  bricks?: Brick[];
  spareBricks?: Brick[];
  brickSetColors?:Color[];
  completeBricks?: Brick[];
  lostBricks?: Brick[];
  theme?: Theme
}
