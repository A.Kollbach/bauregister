import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LoggingService {
  log(message: string): void {
    this.sendToServer('LOG', message);
  }

  info(message: string): void {
    this.sendToServer('INFO', message);
  }

  warn(message: string): void {
    this.sendToServer('WARN', message);
  }

  error(message: string): void {
    this.sendToServer('ERROR', message);
  }

  private sendToServer(level: string, message: string): void {
    // Hier können Sie die Nachricht an einen Server senden oder in einer Datei speichern
    // Zum Beispiel:
    const logMessage = `${new Date().toISOString()} [${level}]: ${message}`;
    console.log(logMessage); // Temporär, bis die Serverintegration abgeschlossen ist
  }
}
