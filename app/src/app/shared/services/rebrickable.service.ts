import {Inject, Injectable, Optional} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiResponse} from "../interfaces/api-response";
import {BASE_PATH, Configuration, LegoService} from "../../core/api/rebrickable-client";
import {BrickSet} from "../interfaces/brick-set";
import {Color} from "../interfaces/color";
import {Brick} from "../interfaces/brick";
import {Theme} from "../interfaces/theme";
import {MiniFigure} from "../interfaces/mini-figure";

@Injectable({
  providedIn: 'root'
})
export class RebrickableService {
  private legoService: LegoService;

  constructor(private httpClient: HttpClient, @Optional() @Inject(BASE_PATH) basePath: string) {
    const config = new Configuration();
    this.legoService = new LegoService(this.httpClient, basePath, config);
  }

  // gibt die Liste aller Farben
  public getColors(): Observable<ApiResponse<Color>> {
    return this.legoService.legoColorsList(undefined, undefined, undefined, undefined, undefined, {})
  }

  // gibt die Farbe mit der gegebenen id
  public getColor(id: string): Observable<Color> {
    return this.legoService.legoColorsRead(id, undefined, undefined, undefined, {});
  }

  // gibt das Lego Set mit der gegebenen Set-Nummer
  public getSet(setNumber: string): Observable<BrickSet> {
    return this.legoService.legoSetsRead(setNumber, undefined, undefined, {});
  }

  public getSets(id: string): Observable<ApiResponse<BrickSet>> {
    return this.legoService.legoSetsList(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, id);
  }

  // gibt eine Liste von Legoteilen zu der gegebenen Setnummer
  public getSetParts(setNumber: string, page?:number, pageSize?: number, observe?:"body", reportProgress?: boolean, options?: {}): Observable<ApiResponse<Brick>> {
    return this.legoService.legoSetsPartsList(setNumber, page, pageSize,observe,reportProgress,options);
  }

  // gibt ein einzelnes Legoteil zu der gegebenen id
  public getPart(id: string): Observable<Brick> {
    return this.legoService.legoElementsRead(id, undefined, undefined, {});
  }

  // Gibt eine Liste von Lego Minifiguren
  public getMinifigs(): Observable<ApiResponse<MiniFigure>> {
    return this.legoService.legoMinifigsList(undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined ,undefined, {})
  }

  // gibt eine Liste von Lego Themen
  public getThemes(): Observable<ApiResponse<Theme>> {
    return this.legoService.legoThemesList(undefined, undefined, undefined, undefined, undefined, {})
  }

  // gibt das thema mit der gegebenen id
  public getTheme(themeId: number): Observable<Theme> {
    return this.legoService.legoThemesRead(themeId, undefined, undefined, undefined, {});
  }
}
