import {Directive, ElementRef, OnInit, Renderer2} from '@angular/core';

@Directive({
  selector: '[brRandomBorder]',
  standalone: true
})
export class RandomBorderDirective implements OnInit {

  constructor(private el: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
    const angle = Math.floor(Math.random() * 360);
    const borderImage = `linear-gradient(${angle}deg, red, orange, yellow, green, blue, indigo, violet) 1`;
    this.renderer.setStyle(this.el.nativeElement, 'border', '0.2em solid');
    this.renderer.setStyle(this.el.nativeElement, 'borderImage', borderImage);
  }
}
