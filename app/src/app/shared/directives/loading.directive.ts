import { Directive, OnDestroy, TemplateRef, ViewContainerRef } from '@angular/core';
import { Store } from '@ngxs/store';
import { Subscription } from 'rxjs';

@Directive({
  standalone: true,
  selector: '[appLoading]',
})
export class LoadingDirective implements OnDestroy {
  private hasView = false;
  private readonly subscription: Subscription;

  constructor(
    private templateRef: TemplateRef<any>,
    private viewContainer: ViewContainerRef,
    private store: Store,
  ) {
    this.subscription = this.store
      .select((state) => state.app.isLoading)
      .subscribe((isLoading) => {
        this.toggleLoading(isLoading);
      });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  private toggleLoading(isLoading: boolean) {
    if (isLoading && !this.hasView) {
      this.viewContainer.clear();
      this.viewContainer.createEmbeddedView(this.templateRef);
      this.addSpinner();
      this.hasView = true;
    } else if (!isLoading && this.hasView) {
      this.viewContainer.clear();
      this.hasView = false;
    }
  }

  private addSpinner() {
    const spinner = document.createElement('div');
    spinner.className = 'spinner-container';
    spinner.innerHTML = '<mat-spinner></mat-spinner>';
    this.viewContainer.element.nativeElement.appendChild(spinner);
  }
}
