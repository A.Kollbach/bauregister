import {Routes} from '@angular/router';
import {SearchComponent} from "./modules/sites/search/search.component";
import {BrickSetBrickListComponent} from "./modules/sites/brick-set-brick-list/brick-set-brick-list.component";

export const routes: Routes = [
  {
    path: '',
    component: SearchComponent
  },
  {
    path: 'bricklist/:id',
    component: BrickSetBrickListComponent
  }
];
