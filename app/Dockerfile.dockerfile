# Basis-Image
FROM node:20.15.0 AS build
# Arbeitsverzeichnis im Container setzen
WORKDIR /app

# Abhängigkeiten installieren
COPY package.json package-lock.json ./
RUN npm install

# App-Quellcode in den Container kopieren
COPY . .

# Angular-App bauen
RUN npm run build

# Port freigeben
EXPOSE 4200

# Angular-Entwicklungsserver starten
CMD ["npm", "start"]
